# Language: Ruby

# Version 2.4.1

### Control version: git (Bitbucket account)

### SLACK Channel

https://onebitcode.slack.com/messages/C2W4443CZ/

### Comandos iniciais

Baixe o projeto com

```git clone https://bitbucket.org/rtoledo/metrate.git```

Entre na pasta do projeto e crie o arquivo .rvmrc, dentro do mesmo digite

```rvm use 2.4.1@metrate --create```

Atualize seu terminal

```cd .```

Instale bundler para controlar as dependências

```gem install bundler```


## Sobre os branchs e etapas a serem seguidas

master, develop, testing

## Como serão os passos a serem seguidos para o desenvolvimento

1 - Atualize seus repositórios (todos). O comando abaixo irá baixar todos os branchs inclusive os em andamento

```git fetch```

2 - Pegue um cartão no trello a ser desenvolvido ou que deseje contribuir. Atenção, sempre faça discussões antes de assumir uma tarefa ou cartão. Não utilize acentos nem pontuações! Por exemplo:

```git checkout -b 1-construir-base-para-desenvolvimento```

3 - Resolva os problemas e sempre tenha testes. SEMPRE!

4 - Finalizando algo, faça o commit de seu branch suba as alterações

```git push origin 1-construir-base-para-desenvolvimento```

5 - Após todas as tarefas feitas do cartão no trello, faça merge com o branch develop e suba também as alterações. Provavelmente você terá que baixar o que existe atualmente no branch develop para evitar conflitos

```git checkout develop```

```git pull origin develop```

```git merge 1-construir-base-para-desenvolvimento```

```git push origin develop```

5 - O código será testado e caso aprovado colocaremos no branch master



A busca de profissionais para atendimento de pacientes em várias áreas, baseando-se em localização, proximidade, especialidade do profissional, convênios aceitos etc é um grande problema hoje em dia.

Existem muitos pacientes que apresentam algum quadro clínico que precisa de atendimento e não consegue achar profissionais para o mesmo. Tanto em sua cidade quanto em regiões próximas. E quando encontram não são conveniados.

Quando encontram existe todo um procedimento de saber se o profissional atende pelo determinado convênio do paciente ou até mesmo particular.

O profissional por sua vez poderá se cadastrar, ou ser cadastrado via administração, com seus devidos campos específicos, pois existem variações para cada tipo de especialidade (referências a nomes de registro por exemplo). E também as clínicas onde atendem, horários, convênios e mais coisas que podemos pensar.

Juntando tudo isto vemos uma forma de pesquisa em que o paciente pode procurar alguém que possa lhe atender, entrar em contato, sim isto será via site e no máximo o profissional receberá um email dizendo que recebeu uma nova mensagem e um link para responder.

Porque isso?! Simples, a idéia final é que o atendimento via chat possa ser classificado e penso que possa existir uma maneira de relatar o atendimento físico mesmo que aconteceu, em uma forma de agenda do profissional, tornando o sistema muito maior do que se espera. Assim o paciente poderá avaliar o procedimento que foi submetido.

Podemos assim ter um ranking de médicos, clínicas, convênios etc…
