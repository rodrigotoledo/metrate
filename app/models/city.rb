class City < ApplicationRecord
  belongs_to :state

  validates :state, presence: true
end
