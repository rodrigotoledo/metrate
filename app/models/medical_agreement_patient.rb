class MedicalAgreementPatient < ApplicationRecord
  belongs_to :medical_agreement
  belongs_to :user
end
