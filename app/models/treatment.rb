class Treatment < ApplicationRecord
  belongs_to :medical_agreement_place_care
  belongs_to :professional, class_name: 'User'
  belongs_to :patient, class_name: 'User'

  validates :medical_agreement_place_care, :professional, :patient, :scheduled_to, :status, :details, presence: true
end
