class User < ApplicationRecord
  rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable


  # Planos de saude que os Pacientes tem
  has_many :medical_agreement_patients
  has_many :medical_agreements, through: :medical_agreement_patients

  # Planos de saude que os Profissionais atendem
  has_many :medical_agreement_professionals
  has_many :medical_agreements_for_treatments, through: :medical_agreement_professionals, class_name: 'MedicalAgreement'


  # Pacientes tem tratamentos a serem atendidos
  has_many :patient_treatments_appointments, foreign_key: :patient_id, class_name: 'Treatment'

  # Profissionais tem tratamentos para atender
  has_many :professional_treatments_appointments, foreign_key: :professional_id, class_name: 'Treatment'

  # patients cities and that prefers attendance
  # has_many :patient_cities
  # has_many :cities, through: :patient_cities
end
