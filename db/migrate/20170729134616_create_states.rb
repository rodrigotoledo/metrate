class CreateStates < ActiveRecord::Migration[5.1]
  def change
    create_table :states do |t|
      t.references :country, foreign_key: true
      t.string :name
      t.string :short_name
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
