class CreateMedicalAgreements < ActiveRecord::Migration[5.1]
  def change
    create_table :medical_agreements do |t|
      t.string :name
      t.boolean :is_public

      t.timestamps
    end
  end
end
