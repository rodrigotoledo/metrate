class CreatePlaceCares < ActiveRecord::Migration[5.1]
  def change
    create_table :place_cares do |t|
      t.references :city, foreign_key: true
      t.float :latitude
      t.float :longitude
      t.string :name

      t.timestamps
    end
  end
end
