class CreateMedicalAgreementPlaceCares < ActiveRecord::Migration[5.1]
  def change
    create_table :medical_agreement_place_cares do |t|
      t.references :medical_agreement, foreign_key: true
      t.references :place_care, foreign_key: true

      t.timestamps
    end
  end
end
