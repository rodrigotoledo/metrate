class CreateMedicalAgreementPatients < ActiveRecord::Migration[5.1]
  def change
    create_table :medical_agreement_patients do |t|
      t.references :medical_agreement, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
