class CreateTreatments < ActiveRecord::Migration[5.1]
  def change
    create_table :treatments do |t|
      t.references :medical_agreement_place_care, foreign_key: true
      t.datetime :scheduled_to
      t.integer :status
      t.references :professional, foreign_key: { to_table: :users }, index: {name: :treatments_professionals}
      t.references :patient, foreign_key: { to_table: :users }, index: {name: :treatments_patients}
      t.text :details

      t.timestamps
    end
  end
end
