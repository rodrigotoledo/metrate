require 'rails_helper'

RSpec.describe HomeController, type: :controller do

  describe "anonymous user" do
    it "should be redirected to signin" do
      get :index
      expect( response ).to redirect_to( new_user_session_path )
    end
  end

  describe "authenticated user" do
    it "should be redirected to root path" do
      sign_in(create(:user))
      get :index
      expect(response).to be_success
    end
  end
end
