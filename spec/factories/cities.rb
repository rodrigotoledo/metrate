FactoryGirl.define do
  factory :city do
    state nil
    sequence(:name) { |n| "City #{n}" }
  end
end
