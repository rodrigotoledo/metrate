FactoryGirl.define do
  factory :country do
    sequence(:name) { |n| "Country #{n}" }
    sequence(:short_name) { |n| "Short Name #{n}" }
    latitude 1.5
    longitude 1.5
  end
end
