FactoryGirl.define do
  factory :place_care do
    city nil
    latitude 1.5
    longitude 1.5
    name "MyString"
  end
end
