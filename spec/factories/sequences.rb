FactoryGirl.define do
  sequence :email do |n|
    "user#{n}@metrate.com.br"
  end

  sequence :name do |n|
    "Name #{n}"
  end
end