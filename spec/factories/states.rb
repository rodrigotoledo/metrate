# encoding utf-8
FactoryGirl.define do
  factory :state do
    country nil
    sequence(:name) { |n| "State #{n}" }
    sequence(:short_name) { |n| "Short Name #{n}" }
    latitude 1.5
    longitude 1.5
  end
end
