FactoryGirl.define do
  factory :treatment do
    medical_agreement_place_care nil
    scheduled_to "2017-07-30 12:02:33"
    status 1
    professional nil
    patient nil
    details "MyText"
  end
end
