FactoryGirl.define do
  factory :user, :class => 'User' do
    email
    sequence(:name) { |n| "User #{n}" }
    password '12345678'
    password_confirmation '12345678'
  end

  factory :user_not_valid, :class => 'User' do
    email
  end
end
