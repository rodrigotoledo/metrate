require 'rails_helper'

RSpec.describe User, type: :model do
  before do
    @user = create(:user)
  end

  describe 'Operations for Administrators' do
    it 'Create user administrator' do
      @user.add_role :administrator
      expect(@user).to be_valid
      expect(@user.has_role?(:administrator)).to be true
    end

    it 'Invalid user administrator' do
      expect(@user).to be_valid
      expect(@user.has_role?(:administrator)).to be false
    end
  end

  describe 'Operations for Patients' do
    it 'Create user patient' do
      @user.add_role :patient
      expect(@user).to be_valid
      expect(@user.has_role?(:patient)).to be true
    end

    it 'Invalid user patient' do
      expect(@user).to be_valid
      expect(@user.has_role?(:patient)).to be false
    end
  end
end
